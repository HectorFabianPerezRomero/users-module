<?php

namespace Drupal\test_august_2021\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestController.
 */
class TestController extends ControllerBase {

  /**
   * Drupal\test_august_2021\Services\TestServiceInterface definition.
   *
   * @var \Drupal\test_august_2021\Services\TestServiceInterface
   */
  protected $testService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->testService = $container->get('test_august_2021.default');
    return $instance;
  }

  /**
   * Index.
   *
   * @return string
   *   Return Hello string.
   */
  public function index() {

    $dbData = $this->testService->getIndexData();

    return [
      '#theme' => 'results_table',
      '#attached' => [
        'library' => ['test_august_2021/js-grid'],
      ],
      '#content' => [
        'data' => json_encode($dbData),
        'fields' => json_encode($this->testService->getFieldsTable()),
      ]
    ];
  }

  /**
   * Function to return user form
   */
  public function getUserForm(){
    $userForm = $this->testService->formBuilder->getForm('Drupal\test_august_2021\Form\FrontForm');

    return [
      '#theme' => 'user_form_template',
      '#form' => $userForm,
    ];
  }

}
