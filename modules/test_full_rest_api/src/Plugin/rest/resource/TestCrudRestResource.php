<?php

namespace Drupal\test_full_rest_api\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\test_august_2021\Services\TestServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "test_crud_rest_resource",
 *   label = @Translation("Test crud rest resource"),
 *   uri_paths = {
 *     "canonical" = "/test-crud/data/{id}",
 *     "https://www.drupal.org/link-relations/create" = "/test-crud/data"
 *   }
 * )
 */
class TestCrudRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\test_august_2021\Services\TestServiceInterface definition.
   *
   * @var \Drupal\test_august_2021\Services\TestServiceInterface
   */
  public $testService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('test_full_rest_api');
    $instance->currentUser = $container->get('current_user');
    $instance->testService = $container->get('test_august_2021.default');
    return $instance;
  }

  /**
   * Responds to GET requests.
   *
   * @param string $payload
   *
   * @return \Drupal\rest\ResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get($payload) {
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $response['data'] = $this->testService->getPlainIndexData($payload);

    return new JsonResponse($response, 200);
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($data){
    $response_status['status'] = false;
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    return $this->testService->processRestCreateRow((array) $data);
  }

  /**
   * Responds to PATCH requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function patch($data){
    $response_status['status'] = false;
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    return $this->testService->processRestUpdateRow((array) $data);
  }

  /**
   * Responds to DELETE requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function delete($data){
    $response_status['status'] = false;
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    $deletedRows = $this->testService->deleteRowById($data);

    $response = [
      'status' => 200,
      'message' => 'Row deleted',
      'data' => [
        'row_id' => $data,
        'rows_affected' => $updatedRows,
      ],
    ];
    return new JsonResponse($response);
  }

}
